package com.stardust.stardust;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShaftDriveApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShaftDriveApplication.class, args);
	}

}
